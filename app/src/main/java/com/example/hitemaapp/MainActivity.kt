package com.example.hitemaapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.hitemaapp.api.ApiService
import com.example.hitemaapp.fragments.SearchFragment
import com.example.hitemaapp.models.User
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val retrofit = Retrofit.Builder()
            .baseUrl("https://jsonplaceholder.typicode.com")
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        val api = retrofit.create(ApiService::class.java)

        //val users = mutableListOf<User>()

        //for(i in 0..10) {
            //users.add(User("test", "test", "test"))
        //}
        //showData(users)

        api.fetchAllUsers().enqueue(object : Callback<List<User>>{
            override fun onResponse(call: Call<List<User>>, response: Response<List<User>>) {
                Log.d("Creation", "Reponse : ${response.toString()}")
            }

            override fun onFailure(call: Call<List<User>>, t: Throwable) {
                Log.d("Creation", "Reponse : fail")
            }

        })

        //injecter fragment search dans le activy main
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.fragment_search_container,SearchFragment())
        transaction.addToBackStack(null)
        transaction.commit()
    }

    private fun showData(users : MutableList<User>) {
        val recycler = findViewById<RecyclerView>(R.id.recyclerView)

        recycler.apply{
            layoutManager = LinearLayoutManager(this@MainActivity)
            adapter = UsersAdapter(users)
        }
    }
}